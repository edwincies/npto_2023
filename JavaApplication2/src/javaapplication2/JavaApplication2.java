package javaaplication2;

import java.util.Random;
import java.io.*;

public class JavaApplication {

    public static void main(String[] args) {
        final int N = 10;
        int []tab = new int[N];

        for (int i = 0; i < N; i++)
            tab[i] = new Random().nextInt(N);

        for (int i = 0; i < N; i++)
            System.out.println( tab[i] + " ");

        int max = tab[0];

        for (int i = 1; i < N; i++) {
            if (tab[i] > max) {
                max = tab[i];
            } 
        }

        System.out.println("MAX: " + max);
        System.out.println("KONIEC PROGRAMU");
    }
}